import React from 'react'

 class AddTodo extends React.Component {
	 constructor(){
		 super();
		 this.state = {
			 todo: ''
		 }
	 }
	
	render() {
		return (
			<div className="addTodoContainer">
				<form onSubmit={(e) => this.addSubmit(e)}>
					<input id="addTodoInput" onChange={(e) => this.updateInput(e)} type='text'></input>
					<button  type="submit">Add Todo</button>
				</form>
				
			</div>
		);
	}
	updateInput = (e) =>{
		this.setState({
			todo: e.target.value
		})
	}
	addSubmit = (e) =>{
		e.preventDefault();
		console.log("submit", this.state)
		this.props.addTodoFn(this.state.todo)
		document.getElementById("addTodoInput").value = "";
	}
}

export default AddTodo
